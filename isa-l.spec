%define isal_libname libisa-l
%define isal_devname libisa-l-devel
Name:                isa-l
Version:             2.31.0
Release:             1
Summary:             Intelligent Storage Acceleration Library
License:             BSD-3-Clause
URL:                 https://github.com/intel/isa-l
Source0:             https://github.com/intel/isa-l/archive/refs/tags/v%{version}.tar.gz
BuildRequires:       nasm  gcc
BuildRequires:       autoconf automake libtool
%description
This package contains the libisal.so dynamic library which contains
a collection of optimized low-level functions targeting storage
applications.

%package -n %{isal_libname}
Summary:             Dynamic library for isa-l functions
License:             BSD-3-Clause
Obsoletes:           %{name} < %{version}
Conflicts:           libisal
%description -n %{isal_libname}
This package contains the libisal.so dynamic library which contains
a collection of optimized low-level functions targeting storage
applications.

%package -n %{isal_devname}
Summary:             ISA-L devel package
Requires:            %{isal_libname}%{?_isa} = %{version}
Provides:            %{isal_libname}-static%{?_isa} = %{version}
%description -n %{isal_devname}
Development files for the %{isal_libname} library.

%prep
%autosetup -p1

%build
if [ ! -f configure ]; then
    ./autogen.sh --no-oshmem
fi
%configure
%make_build

%install
%make_install
find %{?buildroot} -name *.la -print0 | xargs -r0 rm -f

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%check
%make_build check
%make_build test
%make_build perf

%files
%license LICENSE
%{_bindir}/*
%{_mandir}/man1/*
%doc

%files -n %{isal_libname}
%license LICENSE
%{_libdir}/*.so.*

%files -n %{isal_devname}
%license LICENSE
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/pkgconfig/libisal.pc

%changelog
* Fri Dec 20 2024 xu_ping <707078654@qq.com> - 2.31.0-1
- Upgrade version to 2.31.0
  * Fixed memory leaks in unit tests.
  * Added CRC combine example crc_combine_example for multiple polynomials.
  * Added function overview documentation page.
  * Added security file.
  * Added new CRC64 Rocksoft algorithm.
  * Added new AVX512 and AVX2 implementations using GFNI instructions.
  * Added new SVE implementation.
  * Added new AVX2 and AVX512 implementations.
  * Added compress/decompress with dictionary to perf test app.
  * Added isal_zlib_hdr_init() function to initialize the zlib header to 0.
  * gf_vect_mul_base() function now returns an integer, matching the return type
    of gf_vect_mul() function (not a breaking change).

* Sat Feb 25 2023 yaoxin <yaoxin30@h-partners.com> - 2.30.0-2
- Add conflicts for libisal

* Sat Aug 28 2021 liyanan <liyanan32@huawei.com> - 2.30.0-1
- package init
